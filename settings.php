<?php
// Initialize the settings array
$settings = [
    'video1' => [
        'username' => '',
        'title' => ''
    ],
    'video2' => [
        'username' => '',
        'title' => ''
    ],
    'video3' => [
        'username' => '',
        'title' => ''
    ],
    'video4' => [
        'username' => '',
        'title' => ''
    ]
];

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if the import button is clicked
    if (isset($_FILES['csv_file']) && $_FILES['csv_file']['error'] === UPLOAD_ERR_OK) {
        // Open the uploaded CSV file
        $file = fopen($_FILES['csv_file']['tmp_name'], 'r');

        // Read the header row
        $headers = fgetcsv($file);

        // Check if the CSV file has the expected number of columns
        if (count($headers) !== 8) {
            $importError = "Invalid CSV file format. Expected 8 columns.";
        } else {
            // Read the data row
            $dataRow = fgetcsv($file);

            // Combine headers and data to update the settings array
            for ($i = 0; $i < count($headers); $i += 2) {
                $videoNum = substr($headers[$i], 5, 1);
                $settings["video$videoNum"]['username'] = $dataRow[$i];
                $settings["video$videoNum"]['title'] = $dataRow[$i + 1];
            }

            // Set the cookie with the imported settings
            setcookie('settings', json_encode($settings), time() + (86400 * 30), "/");

            $importSuccess = "Settings imported successfully.";
        }

        // Close the CSV file
        fclose($file);
    } elseif (isset($_POST['export'])) {
        // Set the response headers for file download
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="stream_settings.csv"');

        // Open the output stream
        $output = fopen('php://output', 'w');

        // Write the headers
        fputcsv($output, ['video1_username', 'video1_title', 'video2_username', 'video2_title', 'video3_username', 'video3_title', 'video4_username', 'video4_title']);

        // Write the data row
        fputcsv($output, [
            $_POST['video1_username'], $_POST['video1_title'],
            $_POST['video2_username'], $_POST['video2_title'],
            $_POST['video3_username'], $_POST['video3_title'],
            $_POST['video4_username'], $_POST['video4_title']
        ]);

        // Close the output stream
        fclose($output);
        exit;
    } else {
        $emptyUsername = false;

        // Update the settings array with the submitted form data
        foreach ($settings as $key => $video) {
            $videoNum = substr($key, -1);
            $settings[$key]['username'] = $_POST["video{$videoNum}_username"];
            $settings[$key]['title'] = $_POST["video{$videoNum}_title"];

            // Check if the username is empty
            if (empty($settings[$key]['username'])) {
                $emptyUsername = true;
            }
        }

        if ($emptyUsername) {
            $saveError = "Please provide a username for all videos.";
        } else {
            // Set the cookie with the updated settings
            setcookie('settings', json_encode($settings), time() + (86400 * 30), "/");

            $saveSuccess = "Saved settings";
        }
    }
}

// Retrieve the settings from the cookie, if available
$cookieSettings = isset($_COOKIE['settings']) ? json_decode($_COOKIE['settings'], true) : [];

// Merge the cookie settings with the default settings
$settings = array_merge($settings, $cookieSettings);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>YouTube Stream Grid Settings</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body class="settings-body">
    <h1>YouTube Stream Grid Settings</h1>
    <p>This settings page allows you to configure the 2x2 grid of YouTube live streams. Please enter the YouTube channel username and the starting text of the live stream title for each video.</p>

    <?php if (isset($importSuccess)): ?>
        <p class="success-message"><?php echo $importSuccess; ?></p>
        <script>
            setTimeout(function() {
                location.reload();
            }, 2000); // Refresh the page after 2 seconds (2000 milliseconds)
        </script>
    <?php endif; ?>

    <?php if (isset($importError)): ?>
        <p class="error-message"><?php echo $importError; ?></p>
    <?php endif; ?>

    <?php if (isset($saveSuccess)): ?>
        <p class="success-message"><?php echo $saveSuccess; ?></p>
    <?php endif; ?>

    <?php if (isset($saveError)): ?>
        <p class="error-message"><?php echo $saveError; ?></p>
    <?php endif; ?>

    <form class="settings-form" method="post" enctype="multipart/form-data">
        <?php foreach ($settings as $key => $video): ?>
            <fieldset>
                <legend>Video <?php echo substr($key, -1); ?></legend>
                <label class="settings-label" for="<?php echo $key; ?>_username">Channel Username:</label>
                <input class="settings-input" type="text" id="<?php echo $key; ?>_username" name="<?php echo $key; ?>_username" value="<?php echo $video['username']; ?>" placeholder="@channel">
                <label class="settings-label" for="<?php echo $key; ?>_title">Stream Title Starts With:</label>
                <input class="settings-input" type="text" id="<?php echo $key; ?>_title" name="<?php echo $key; ?>_title" value="<?php echo $video['title']; ?>" placeholder="Stream Title">
            </fieldset>
        <?php endforeach; ?>
        <input type="submit" value="Save" class="settings-submit">
        <label for="csv_file" class="settings-import">Import Settings</label>
        <input type="file" id="csv_file" name="csv_file" accept=".csv" style="display: none;">
        <input type="submit" name="export" value="Export Settings" class="settings-export">
        <input type="button" value="Close" class="settings-export" onclick="window.location.href='https://tv.nor.ski/'">
    <script>
        // Trigger file upload when the "Import Settings" label is clicked
        document.querySelector('.settings-import').addEventListener('click', function() {
            document.getElementById('csv_file').click();
        });

        // Submit the form when a file is selected
        document.getElementById('csv_file').addEventListener('change', function() {
            document.querySelector('.settings-form').submit();
        });
    </script>
</body>
</html>