// Function to fetch settings from cookies
function fetchSettings() {
    const settingsCookie = document.cookie.split('; ').find(row => row.startsWith('settings='));
    if (settingsCookie) {
        const settingsJSON = settingsCookie.split('=')[1];
        return JSON.parse(decodeURIComponent(settingsJSON));
    }
    return null;
}

// Function to get a URL parameter value by name
function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

// Function to create a YouTube iframe with the specified video ID
function createYouTubeIframe(videoId) {
    const iframe = document.createElement('iframe');
    iframe.src = `https://www.youtube.com/embed/${videoId}?autoplay=1&mute=1&controls=1&showinfo=0&autohide=1&vq=medium`;
    iframe.setAttribute("allow", "autoplay");
    return iframe;
}

// Fetch the latest live stream link
async function getLatestLiveStreamLink(username, titleStartsWith) {
    if (!username) {
        throw new Error("No username provided");
    }

    try {
        const url = `https://www.youtube.com/${username}/streams`;
        const response = await fetch(`fetch_stream.php?url=${encodeURIComponent(url)}`);
        const html = await response.text();

        if (titleStartsWith) {
            // Replace placeholders with actual date values
            const date = new Date();
            const day = String(date.getDate()).padStart(2, '0');
            const month = String(date.getMonth() + 1).padStart(2, '0');
            const year = date.getFullYear();
            titleStartsWith = titleStartsWith.replace('DD', day).replace('MM', month).replace('YYYY', year);

            // Use a regular expression to find the video ID that corresponds to the title
            const regex = new RegExp(`"text":"${titleStartsWith}.*?"videoId":"(.*?)"`);
            const match = html.match(regex);

            if (match) {
                return match[1];
            }
        }

        // If no title is provided or a matching title is not found, fetch the first live stream
        const firstLiveStreamRegex = /"videoId":"(.*?)"/;
        const firstLiveStreamMatch = html.match(firstLiveStreamRegex);

        return firstLiveStreamMatch ? firstLiveStreamMatch[1] : null;
    } catch (error) {
        console.error(error);
        throw error;
    }
}

// Initialize the window onload handler
window.onload = async function() {
    const settings = await fetchSettings();

    if (!settings || !settings.video1 || !settings.video2 || !settings.video3 || !settings.video4) {
        const errorMessage = document.createElement('div');
        errorMessage.innerHTML = `<img src="/images/youtube-icon-logo.svg" alt="logo" class="logo" /><br />Error: Please <a href="/settings/" style="color: blue; text-decoration: underline;">check settings</a>`;
        errorMessage.style.position = 'absolute';
        errorMessage.style.left = '50%';
        errorMessage.style.top = '50%';
        errorMessage.style.transform = 'translate(-50%, -50%)';
        errorMessage.style.fontSize = '24px';
        errorMessage.style.color = 'red';
        document.body.appendChild(errorMessage);
        return;
    }

    // Loop through settings to get video IDs
    const videoIds = {};
    for (let i = 1; i <= 4; i++) {
        const videoInfo = settings[`video${i}`] || {};
        try {
            videoIds[`video${i}`] = await getLatestLiveStreamLink(videoInfo.username, videoInfo.title);
        } catch (error) {
            console.error(`Error fetching video ID for video${i}:`, error);
            videoIds[`video${i}`] = null;
        }
    }

    // Loop through video IDs, creating and appending iframes for each one
    for (const [videoElementId, videoId] of Object.entries(videoIds)) {
        if (videoId) {
            document.getElementById(videoElementId).appendChild(createYouTubeIframe(videoId));
        } else {
            const errorDiv = document.createElement('div');
            errorDiv.innerHTML = "Error loading stream";
            errorDiv.style.textAlign = "center";
            errorDiv.style.marginTop = "50%";
            document.getElementById(videoElementId).appendChild(errorDiv);
        }
    }
};