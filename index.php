<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Cache-Control" content="no-store, must-revalidate" />
    <title>YouTube Quad Live</title>
    <link href="css/style.css" rel="stylesheet">
    <script src="https://www.youtube.com/iframe_api"></script>
</head>
<body>

<div class="video-container">
    <div class="video" id="video1"></div>
    <div class="video" id="video2"></div>
    <div class="video" id="video3"></div>
    <div class="video" id="video4"></div>
</div>
<script src="js/streamGrid.js"></script>

</body>
</html>