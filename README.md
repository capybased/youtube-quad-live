# YouTube Live Streams Grid

This project displays live streams from multiple YouTube channels in a 2x2 grid. It automatically fetches the latest live stream links for the specified channels and titles and embeds them in iframes.

## Features

- **Automatic Live Stream Link Retrieval**: Automatically fetches the latest live stream links for the specified channels and titles.
- **Responsive Grid Layout**: Displays the live streams in a 2x2 grid that fills the viewport.
- **Customizable Channels and Titles**: Customize the channels and titles via a settings page. The titles are used in regular expressions to automatically fetch the corresponding live streams.

## Prerequisites

- A web server with PHP support to host the files.

## Installation

1. Clone the repository or download the ZIP file.
2. Upload the files to your web server.
3. Modify the `fetch_stream.php` file if needed to comply with your server's configuration.
4. Open `settings.php` in your browser to configure the channels and titles.
5. Open `index.php` in your browser to view the live streams.

## Usage

You can customize the channels and titles by navigating to the `settings.php` page. Specify the URLs and title patterns for the channels you want to display. The title supports using todays date by typing in DD/MM/YYYY or other date variations.

## Compliance with YouTube's Terms of Service

If you choose to use this code, please make sure that you are not in violation of YouTube's Terms of Service. It is the user's responsibility to comply with all applicable laws and regulations.

## Limitations

- This project relies on scraping YouTube's website, which is a fragile approach. The structure of the HTML can change at any time, causing the code to break.
- Consider using the YouTube API or another more reliable method if you need a robust solution.

## Contributing

If you'd like to contribute, please fork the repository and use a feature branch. Pull requests are warmly welcome.

## License

This script is released under the MIT License.

## Acknowledgments

- Thanks to YouTube for providing the platform for live streaming.
